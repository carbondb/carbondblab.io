# Carbon DB Website

Documentation and main web resources for the Carbon DB project. It covers
the top level documentation, governance and specification.

## Get started

The site is generated using [Hugo](https://gohugo.io/) so the first step is
to [install it](https://gohugo.io/getting-started/installing/). Once Hugo is
installed, you should just be able to serve the site:

```bash
hugo serve
```

GitLab CI is configured in the repository so that any time you push, it will
attempt to re-generate the site.

## Repository structure

The theme of the website is in the `themes/carbondb` folder. This is where
you will find the layout and styling files.

The content is in the `content` folder and is a set of Markdown files that
follow the [CommonMark](https://commonmark.org/) specification with added
[Hugo shortcodes](https://gohugo.io/content-management/shortcodes/). At
present, the only supported shortcodes are the
[built-in ones](https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes).

## License

The code of the Carbon DB website is licensed under the Apache 2.0 license.
For the full text, see the
[Apache 2.0 license copy](https://gitlab.com/carbondb/carbondb.gitlab.io/-/raw/main/LICENSE.txt).
