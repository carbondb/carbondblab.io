---
title: "Project Governance"
draft: false
---

This project is in its infancy and so is its governance. That said, here's a
start for how it all works.

## Roles

Contributors can play the following roles in the project:

### Lead Maintainer

As this project is currently led by [imbytech](https://www.imbytech.com/),
the project lead and lead maintainer is imby's CTO, Bruno Girin. In addition
to the responsibilities of the Core Maintainers, the Lead Maintainer is
responsible for:
- The overall solution design
- Prioritising issues
- Reviewing issues and merge requests from Core Maintainers and Contributors

### Core Maintainers

The Core Maintainers are currently the [imbytech](https://www.imbytech.com/)
team and their responsibilities include:
- Implementing core functionality
- Reviewing issues and merge requests from Contributors

### Maintainers

The Maintainer role refers to anyone who is a Lead or Core Maintainer. All
Maintainers are also Contributors.

### Contributors

Contributors are all people who contribute to the project, whether it be through
issues or merge requests. Contributions can include documentation, code, or
anything that improves the project. No particular qualification is required to
become a contributor.

### Community Leaders

Community Leaders are responsible for ensuring the community around the project
is welcoming and safe for every Contributor. They are responsible for enforcing
the [Code of Conduct]({{< relref code_of_conduct.md >}}).

There is currently a single Community Leader who is also the Lead Maintainer.
As the project grows, we would like this to change in order to spread the
responsiblities of the project and ensure that everyone is held accountable.

## Governance Changes

Any changes to the list of roles or any other governance aspect should be done
by opening
[a project issue in GitLab](https://gitlab.com/carbondb/carbondb.gitlab.io/-/issues)
and tagging it with the `Governance` label. Explain the aspect of governance you
would like to change and why.

This project is currently run by a private company, imby and the Lead Maintainer
is a single individual. This is appropriate for the moment but will need to change
if this project is used more widely. We welcome any suggestion on how to
implement proper governance and transition to a more open model in the future.

## Policies and Procedures

We don't yet have any policies and procedures beyond the
[Code of Conduct]({{< relref code_of_conduct.md >}}) at present.
