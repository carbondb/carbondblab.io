---
title: "4. Feeds"
draft: false
---

## 2.1. Object Model

A feed is a component that is able to receive data in an external data format,
process it and populate timeseries linked to a number of meters using that
data. Feed level attributes are:
- The meter schemes it supports.
- Definition of what metrics it supports and in what units.
- Metadata used to identify how to query the feed to identify whether it
  supports a specific meter.
- Metadata used to identify how the feed receives data and in what format.
- Mapping between external format and meter characteristics, in particular
  metric labels, units, error conditions, etc.

{{< aside warning >}}
The fields a feed object contains are currently very vague and will be
clarified as individual feeds are implemented. The expectation is that
it should include data like:
- the transport protocol (FTP, HTTP, etc)
- the data format (CSV, JSON, etc) with additional semantic description
- if the feed is push or pull
    - pull URL and parameters
- frequency of updates and scheduling options
{{< /aside >}}

The feed API supports the creation, update and retrieval of this data.

## 4.2. Operations

{{< aside note >}}
The security requirements and permission format will require more research.
{{< /aside >}}

### 4.2.1. List Meters

List all the feeds in the database. The actual list will be filtered to
only return feeds for which the user has read permissions.

- Permission: `list:feeds`
- Verb: `GET`
- Path: `/feeds`

### 4.2.2. Create Feed

Create a new feed.

- Permission: `create:feeds`
- Verb: `POST`
- Path: `/feeds`

### 4.2.3. Read Feed

Read the feed level attributes of the given feed.

- Permissions: `read:feeds:{id}`, `read:feeds:tag={label}`
- Verb: `GET`
- Path: `/feeds/{id}`

### 4.2.4. Update Feed

Update the feed level attributes of the given feed.

- Permissions: `update:feeds:{id}`, `update:feeds:tag={label}`
- Verb: `PUT`
- Path: `/feeds/{id}`

### 4.2.5. Delete Feed

Delete a feed and all associated records.

- Permissions: `delete:feeds:{id}`, `delete:feeds:tag={label}`
- Verb: `DELETE`
- Path: `/feeds/{id}`

### 4.2.6. Post Data to Feed

Post data to a feed in the data format expected by the feed.

- Permissions: `update:feeds-data:{id}`, `update:feeds-data:tag={label}`
- Verb: `POST`
- Path: `/feeds/{id}/data`
