---
title: "1. Access Control"
draft: false
---

{{< aside warning >}}
The access control and security requirements are very basic at present and
will require more research.
{{< /aside >}}

## 1.1. Basic Principles

Access to Carbon DB operations is controlled through authorization tokens.
All requests should contain an `Authorization` header that the service is
able to resolve into a set of permissions.

## 1.2. OpenID Connect

When using OpenID Connect, the service should call the `userinfo` endpoint
with the token passed in the `Authorization` header. Permissions should be
available in the `permissions` or `scope` claim.
