---
title: "2. Timeseries"
draft: false
---

## 2.1. Object Model

A timeseries stores a set of timestamped records, each with a numeric value.
Some attributes of a series are generic to all records and drive how the
records are manipulated. Timeseries level attributes are:
- The aggregation type of the series, i.e. whether the records are discrete
  or cummulative. In a discrete series, records are aggregated by calculating
  the average, while in a cummulative series, records are aggregated by
  calculating the sum.
- The sampling interval, which indicates at what frequency values are sampled.
  This is an indicative value for discrete series and a normative value for
  cummulative series.
- The unit the values of the records are expressed in. This can be used by
  client processes to identify how to process the series or display it.

Record level attributes are:
- A timestamp indicating the time at which the value was sampled for discrete
  readings. For cummulative or aggregated readings, it is the start of the
  period that the reading covers, the length of the period being indicated by
  the series' sampling interval.
- A numeric value or `null` if no value is recorded.

The timeseries API supports the creation, update and retrieval of this data.
It also supports some simple operations on retrieval:
- Time slice: only return records for a given period of time.
- Down-sample: aggregate records to a longer sampling period than the one
  that is inherent to the series.
- Timezone shift: apply a timezone to all timestamps to shift them to local
  time.
- Or any combination of the above.

## 2.2. Operations

{{< aside note >}}
The security requirements and permission format will require more research.
{{< /aside >}}

### 2.2.1. List Timeseries

List all the series in the database. The actual list will be filtered to
only return series for which the user has read permissions.

- Permission: `list:series`
- Verb: `GET`
- Path: `/series`

### 2.2.2. Create Timeseries

Create a new timeseries.

- Permission: `create:series`
- Verb: `POST`
- Path: `/series`
- Parameters:
    - period
    - unit

### 2.2.3. Read Timeseries

Read the timeseries level attributes of the given timeseries.

- Permissions: `read:series:{id}`, `read:series:tag={label}`
- Verb: `GET`
- Path: `/series/{id}`

### 2.2.4. Update Timeseries

Update the timeseries level attributes of the given timeseries.

- Permissions: `update:series:{id}`, `update:series:tag={label}`
- Verb: `PUT`
- Path: `/series/{id}`
- Parameters:
    - period
    - unit

### 2.2.5. Delete Timeseries

Delete a timeseries and all associated records.

- Permissions: `delete:series:{id}`, `delete:series:tag={label}`
- Verb: `DELETE`
- Path: `/series/{id}`

### 2.2.6. Create Records

Add records to a timeseries. If records with the same timestamp already
exist, they will be replaced by the new records. An exiting record can
be deleted by settig its value to `null`.

- Verb: `POST`
- Path: `/series/{id}/records`
- Parameters:
    - records: array of records, each containing:
        - t: timestamp
        - v: value

### 2.2.7. Read Records

Read all raw records from a timeseries. This operation may be restricted
by the implementation.

- Verb: `GET`
- Path: `/series/{id}/records`

### 2.2.8. Read a Time Slice of Records

Read records between the `start` and `end` values, downsample to the given
`interval` and shift by the given timezone.

- Verb: `GET`
- Path: `/series/{id}/records/{start}/{end}/{interval}`
- Parameters:
    - tz: the timezone to apply

The `start` and `end` parameters form the two parts of a
[ISO 8601 time interval](https://en.wikipedia.org/wiki/ISO_8601#Time_intervals).
Either `start` or `end` can be a duration but not both.
The `interval` parameter is a
[ISO 8601 duration](https://en.wikipedia.org/wiki/ISO_8601#Durations) that
indicates the sampling interval of the resulting slice.
Each of those three parameters can be replaced with a special value of `-` that
indicates:
- `start`: the slice period starts at the start of recording
- `end`: the slice period finishes at the end of recording
- `interval`: the slice returns raw data

The path `/series/{id}/records/-/-/-` should return the same as
`/series/{id}/records` with the same limitations.

The `end` value can also be the special word `now` to read all records up to
the current timestamp. This value may be different from the end of recording
and is not fixed. Two calls to the API with identical parameters may return
different results when the `now` keyword is used.

### 2.2.9. Stream a Time Slice of Records

Open a websocket stream for that timeseries over which record slices can be
requested.

- Verb: `Web Socket`
- Path: `/series/{id}/stream`
