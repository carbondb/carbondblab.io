---
title: "3. Meters"
draft: false
---

## 3.1. Object Model

A meter groups a series of related metrics. Each metric is backed by a
timeseries. Meter level attributes are:
- A set of identifiers.
- A set of metrics.

{{< aside note >}}
We will probably need a meter type or template so that we can specify how
a given meter is created and what metrics should be provided. This will
interact with the feed because a given feed may be able to provide some
metrics but not others.
{{< /aside >}}

Identifier level attributes are:
- The identification scheme, such as `mpan`.
- The identifier in the scheme.

{{< aside note >}}
Identifiers such as MPAN also come with additional metadata that describe the
meter. This could be stored as part of the identifier or the meter itself.

The identifier scheme is also related to possible feeds for the meter, e.g.
the MPAN scheme can use the SMETS or the AMR feed.
{{< /aside >}}

Metric level attributes are:
- The kind of metric or its unit
- A label
- A timeseries URI

{{< aside note >}}
If we have a template mechanism, the template should take the property and the
meter should then store the unit based on what the corresponding feed can
supply. If not, the meter should just store the property.

The property is the kind of quantity measured by the metric, such as energy.
The same propery can be expressed in different units that can be converted
from one to another, e.g. energy can be expressed in Joules, kWh, MWh, etc.
UCUM calls it `property` or `kind`.

The label should have a standard naming scheme that is backed by a data
dictionary.

The timeseries URI will be a simple ID that can be used by the
[timeseries API]({{< relref 2-timeseries >}}) initially. It will become
more complicated in the future when we support virtual meters.
{{< /aside >}}


The meter API supports the creation, update and retrieval of this data.

## 3.2. Operations

{{< aside note >}}
The security requirements and permission format will require more research.
{{< /aside >}}

### 3.2.1. List Meters

List all the meters in the database. The actual list will be filtered to
only return meters for which the user has read permissions.

- Permission: `list:meters`
- Verb: `GET`
- Path: `/meters`

### 3.2.2. Create Meter

Create a new meter.

- Permission: `create:meters`
- Verb: `POST`
- Path: `/meters`

### 3.2.3. Read Meter

Read the meter level attributes of the given meter.

- Permissions: `read:meters:{id}`, `read:meters:tag={label}`
- Verb: `GET`
- Path: `/meters/{id}`

### 3.2.4. Update Meter

Update the meter level attributes of the given meter.

- Permissions: `update:meters:{id}`, `update:meters:tag={label}`
- Verb: `PUT`
- Path: `/meters/{id}`

### 3.2.5. Delete Meter

Delete a meter and all associated records.

- Permissions: `delete:meters:{id}`, `delete:meters:tag={label}`
- Verb: `DELETE`
- Path: `/meters/{id}`
