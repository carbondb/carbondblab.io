## What is it?

Carbon DB is a specification and an implementation of a timeseries database
dedicated to storing energy and carbon data so that anyone can measure their
usage and reduce it.

The aim is to provide several components:
- A full API specification that enables anyone with the right technical skills
  to build a compatible implementation.
- A reference implementation that enables anyone to spin up their own version
  of Carbon DB for their own use.
- Some client libraries to use the data stored in an instance of Carbon DB.
- A technology compatibility kit to test your implementation against the spec.

## Why does it exist?

During 10 years building energy and carbon management solutions for the built
environment, we noticed a few things:
- Most timeseries database solutions on the market are designed for analytics
  and don't deal very well with the particularities of energy data.
- Most organisations that deal with that type of data end up building their
  own custom solution.
- None of those solutions integrate seamlessly.
- Very few of them handle consent and privacy processes, meaning that once
  your energy data is in a cloud database, you either don't have access to it
  or everybody does (note: this is a simplification of the problem but you get
  the gist).

So we decided to build an open source solution with a well defined spec that
would allow anyone to either use our reference implementation or build a
compatible implementation, should they use a different technology stack than
what we do.

## How to use it?

Once our [Python reference implementation](https://py.carbondb.tech) is ready,
you will be able to download it and run it on your own computer or in the
cloud.

## How to build your own?

Once our [specification]({{< relref specs >}}) is ready, you will be able to
create a service that implements it.

## How to contribute?

### High level process

There are a number of ways you can contribute to the project and there is no
requirement to know how to code to do so: any contribution, from advising us
of a typo in the documentation to delivering a complete section of the project
are welcome. All resources are hosted on [GitLab](https://gitlab.com/) so you
will need a GitLab account to contribute. For all areas, the process follows
the following steps:

1. A Contributor opens an issue in GitLab describing the problem or the feature
   needed
2. A Maintainer reviews the issue and assigns it to a Contributor
3. A Contributor fixes the problem or implements the feature
4. A Maintainer merges the change into the main documentation or code branch

All contributions are subject to our [Code of Conduct]({{< relref code_of_conduct.md >}})
so please make sure you read it and are comfortable with it before contributing.

### Project governance

As the project is very new, its governance is as simple as we could make it and
is captured in a [Governance document]({{< relref governance.md >}}). This
document defines the different roles in the project, in particular the
Maintainer and Contributor roles referenced above. We expect this document to
get more complex as the number of Contributors grows.

### Main documentation and specifications

The main documentation is the point of entry to the project so it is essential
that it be clearly worded and able to guide newcomers. Valuable contributions
include:
- Reviewing the documentation for typos and clarity
- Suggesting more complex changes that would help a newcomer understand the
  project better
- Contributing diagrams and other visual resources
- Styling: this website is a bit black and white at present
- Logo: we are currently using the IMBY logo, this project should have its
  own separate logo at some point

If you have experience of writing technical specifications, you can also
contribute to the details of the specs:
- Clarify the information to ensure that readers who want to implement the
  API can do so
- Address the notes and warnings (see below)

{{< aside note >}}
This is an example of a note.
{{< /aside >}}

{{< aside warning >}}
This is an example of a warning.
{{< /aside >}}

### Reference implementation

If you want to get more involved with code, you can contribute to the
[Python reference implementation](https://py.carbondb.tech). Please see
the "How to contribute?" section in that part of the website for full
details.
